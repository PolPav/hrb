<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {

    return $request->user();
});

Route::group(['middleware' => 'auth:api'], function() {

    Route::get('{dip}', 'DipController@index')->where(['dip' => '[0-9_a-zA-Z]+']);
    Route::get('{dip}/{id}', 'DipController@show')->where(['dip' => '[0-9_a-zA-Z]+']);
    Route::post('{dip}', 'DipController@store')->where(['dip' => '[0-9_a-zA-Z]+']);
    Route::put('{dip}/{id}', 'DipController@update')->where(['dip' => '[0-9_a-zA-Z]+']);
    Route::delete('{dip}/{id}', 'DipController@delete')->where(['dip' => '[0-9_a-zA-Z]+']);

});

Route::post('register', 'Auth\RegisterController@register');
Route::get('register', 'Auth\RegisterController@register');
Route::post('login', 'Auth\LoginController@login');
Route::get('login', 'Auth\LoginController@login');
Route::get('logout', 'Auth\LoginController@logout');
