<?php

namespace App\Providers;

use Illuminate\Contracts\Auth\Access\Gate as GateContract;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\DB;
use App\User;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        'App\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @param\Illuminate\Contracts\Auth\Access\Gate $gate
     * @return void
     */
    public function boot(GateContract $gate)
    {

        $this->registerPolicies();

//        $dip = explode('/', Request::getPathInfo());
//
//        $dipName = $dip[2];

        $gate->define('read-dip', function (User $user){

            $dip = explode('/', Request::getPathInfo());
            $dipName = $dip[2];

            $crud = DB::table('users')
                ->join('user_group', 'users.id', '=', 'user_group.user_id')
                ->join('group_crud', 'user_group.group_id', '=', 'group_crud.group_id')
                ->join('crud', 'group_crud.crud_id', '=', 'crud.id')
                ->select('crud.title')
                ->distinct()
                ->where('users.id', $user->id)
                ->get();

            foreach ($crud as $access) {

                if ($access->title == $dipName.'_read') {

                    return true;
                }
            }

            return false;
        });

        $gate->define('create-dip', function (User $user){

            $dip = explode('/', Request::getPathInfo());
            $dipName = $dip[2];

            $crud = DB::table('users')
                ->join('user_group', 'users.id', '=', 'user_group.user_id')
                ->join('group_crud', 'user_group.group_id', '=', 'group_crud.group_id')
                ->join('crud', 'group_crud.crud_id', '=', 'crud.id')
                ->select('crud.title')
                ->distinct()
                ->where('users.id', $user->id)
                ->get();

            foreach ($crud as $access) {

                if ($access->title == $dipName.'_create') {

                    return true;
                }
            }

            return false;
        });

        $gate->define('update-dip', function (User $user){

            $dip = explode('/', Request::getPathInfo());
            $dipName = $dip[2];

            $crud = DB::table('users')
                ->join('user_group', 'users.id', '=', 'user_group.user_id')
                ->join('group_crud', 'user_group.group_id', '=', 'group_crud.group_id')
                ->join('crud', 'group_crud.crud_id', '=', 'crud.id')
                ->select('crud.title')
                ->distinct()
                ->where('users.id', $user->id)
                ->get();

            foreach ($crud as $access) {

                if ($access->title == $dipName.'_update') {

                    return true;
                }
            }

            return false;
        });

        $gate->define('delete-dip', function (User $user){

            $dip = explode('/', Request::getPathInfo());
            $dipName = $dip[2];

            $crud = DB::table('users')
                ->join('user_group', 'users.id', '=', 'user_group.user_id')
                ->join('group_crud', 'user_group.group_id', '=', 'group_crud.group_id')
                ->join('crud', 'group_crud.crud_id', '=', 'crud.id')
                ->select('crud.title')
                ->distinct()
                ->where('users.id', $user->id)
                ->get();

            foreach ($crud as $access) {

                if ($access->title == $dipName.'_delete') {

                    return true;
                }
            }

            return false;
        });
    }
}
