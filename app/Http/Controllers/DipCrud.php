<?php
/**
 * Created by PhpStorm.
 * User: PolPav
 * Date: 13.04.2018
 * Time: 9:27
 */

namespace App\Http\Controllers;

use Gate;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;


abstract class DipCrud
{

    /**
     * Read action
     *
     * @param $dipName
     * @return object
     */
    public function index($dipName)
    {
        if(Gate::denies('read-dip')){

            return response()->json(['result' => 'access denied'], 403);
        }

        if(!empty($dipName) && Schema::hasTable($dipName)){

            $dip = DB::table($dipName)
                ->where('TRASH', 0)
                ->get();

            return response()->json(['result' => 'success', 'data' => $dip], 200);

        } else {

            return response()->json(['result' => 'fail', 'error' => 'Not Found'], 404);
        }
    }

    /**
     * Read action record by id
     *
     * @param $dipName
     * @param $id
     * @return object
     */
    public function show($dipName, $id)
    {
        if(Gate::denies('read-dip')){

            return response()->json(['result' => 'access denied'], 403);
        }

        if(!empty($dipName) && Schema::hasTable($dipName)) {

            $dip = DB::table($dipName)
                ->where('x_id', $id)
                ->where('TRASH', 0)
                ->get();

            return response()->json(['result' => 'success', 'data' => $dip], 200);

        } else {

            return response()->json(['result' => 'fail', 'error' => 'Not Found'], 404);
        }
    }

    /**
     * Create action
     *
     * @param $dipName
     * @param Request $request
     * @return object
     */
    public function store($dipName, Request $request)
    {
        if(Gate::denies('create-dip')){

            return response()->json(['result' => 'access denied'], 403);
        }

        if(!empty($dipName) && Schema::hasTable($dipName)) {

            $input = $request->except(['api_token']);
            $dip = DB::table($dipName)->insert($input);

            return response()->json(['result' => 'success', 'data' => $dip], 201);

        } else {

            return response()->json(['result' => 'fail', 'error' => 'Not Found'], 404);
        }
    }

    /**
     * Update action
     *
     * @param $dipName
     * @param $id
     * @param Request $request
     * @return object
     */
    public function update($dipName, $id, Request $request)
    {

        if(Gate::denies('update-dip')){

            return response()->json(['result' => 'access denied'], 403);
        }

        if(!empty($dipName) && Schema::hasTable($dipName)) {

            $input = $request->except(['api_token']);
            $res = DB::table($dipName)
                ->where('x_id', $id)
                ->where('TRASH', 0)
                ->update($input);

            return response()->json(['result' => 'success', 'data' => $res], 200);

        } else {

            return response()->json(['result' => 'fail', 'error' => 'Not Found'], 404);
        }
    }

    /**
     * Delete action
     *
     * @param $dipName
     * @param $id
     * @return object
     */
    public function delete($dipName, $id)
    {
        if(Gate::denies('delete-dip')){

            return response()->json(['result' => 'access denied'], 403);
        }

        if(!empty($dipName) && Schema::hasTable($dipName)) {

            DB::table($dipName)
                ->where('x_id', $id)
                ->update(['TRASH' => 1]);

            return response()->json(['result' => 'success'], 204);

        } else {

            return response()->json(['result' => 'fail', 'error' => 'Not Found'], 404);
        }
    }
}